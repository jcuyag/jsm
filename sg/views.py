from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'contents/home.html')

def service(request):
    return render(request, 'contents/service.html')

def contact(request):
    return render(request, 'contents/contact.html')